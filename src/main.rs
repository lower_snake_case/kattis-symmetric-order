fn sort_names(
    names: std::collections::linked_list::LinkedList<String>,
) -> std::collections::linked_list::LinkedList<String> {
    let mut sorted_names: std::collections::linked_list::LinkedList<String> =
        std::collections::linked_list::LinkedList::new();
    let mut i = if names.len() % 2 == 0 { 1 } else { 0 };

    for name in names.iter().rev() {
        if i % 2 == 0 {
            sorted_names.push_front(name.clone());
        } else {
            sorted_names.push_back(name.clone());
        }
        i += 1;
    }

    sorted_names
}

fn sort_and_add_to_result(
    names: std::collections::linked_list::LinkedList<String>,
    result: &mut std::collections::linked_list::LinkedList<String>,
) {
    let sorted_names = sort_names(names);

    for name in sorted_names.iter() {
        result.push_back(name.clone());
    }
}

fn main() {
    let mut buf: String = String::new();
    let mut amount_of_names: i32;
    let mut result: std::collections::linked_list::LinkedList<String> =
        std::collections::linked_list::LinkedList::new();
    let mut index: i32 = 0;

    std::io::stdin().read_line(&mut buf).unwrap();

    amount_of_names = buf.trim().parse::<i32>().unwrap();

    while amount_of_names != 0 {
        let mut names: std::collections::linked_list::LinkedList<String> =
            std::collections::linked_list::LinkedList::new();

        for _i in 0..amount_of_names {
            buf.clear();
            std::io::stdin().read_line(&mut buf).unwrap();
            names.push_back(buf.trim().parse().unwrap());
        }

        result.push_back(format!("SET {}", index + 1));

        sort_and_add_to_result(names, &mut result);

        buf.clear();
        std::io::stdin().read_line(&mut buf).unwrap();
        amount_of_names = buf.trim().parse::<i32>().unwrap();

        index += 1;
    }

    for name in result.iter() {
        println!("{}", name);
    }
}
